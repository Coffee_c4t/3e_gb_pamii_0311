import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaImoveisPageRoutingModule } from './lista-imoveis-routing.module';

import { ListaImoveisPage } from './lista-imoveis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaImoveisPageRoutingModule
  ],
  declarations: [ListaImoveisPage]
})
export class ListaImoveisPageModule {}
